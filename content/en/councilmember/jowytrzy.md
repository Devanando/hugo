---
title : "Joanna Wytrzyszczewska"
description: ""
draft: false
weight: 5
---

{{< imageshortcode src="images/jowytrzy.jpg" width="400" height="400">}}

Founding and joining the Council was my way of helping the community grow. I find it very important that students’ opinions are heard and taken into account. I believe having a council makes it easier for the staff and the students to communicate since the Council puts effort to aggregate all the students’ ideas and opinions and present them to bocal as a coherent whole. 

I must say I’m really happy to be a part of this! It adds variety to my studies at Codam, lets me meet new people and test my skills in new situations. Being deputy chair also lets me think of a big picture of the council - the structure and its goals. 

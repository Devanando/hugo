---
title : "Liewe Gutter"
description: ""
draft: false
weight: 2
---

{{< imageshortcode src="images/lgutter.jpg" width="400" height="400">}}

Codam has genuinely changed my life, and I believe it has the potential to do the same for many others. But a lot has changed since 2018, which is expected and needed, but In some respects I felt Codam was slowly losing some of its potential. So when I heard there was a student council being set up, I saw this as the perfect opportunity to give back to the school that gave me so much. I was very pleased to discover the other members of the council were just as motivated to help Codam be the best it could be, and even in the first term of the Council, I feel we’ve been able to help Codam in meaningful ways.

This motivated me to continue in the council, and I decided to run for Chair for the second term, both to guide the council as best I could, and because this is a very valuable learning experience. With another great set of members, I believe we can once again impact Codam in a meaningful way!

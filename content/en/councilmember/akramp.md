---
title : "Angeli Kramp"
description: ""
draft: false
weight: 4
---

{{< imageshortcode src="images/akramp.jpg" width="342" height="400">}}

I joined the student council for the same reason I helped setting it up; I think a student council is essential to every school, to make sure all students have the best experience while studying. Bocal does a great job on its own, but often lacks input from students and doesn't directly see the consequences of decisions they make. The student council will bridge this gap and assist and cooperate with Bocal.
I signed up for the PR position because I love being creative and feel like the students aren’t aware enough of the council's efforts and achievements. I hope to work on this and be useful to the council in my own way! uwu
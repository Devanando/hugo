---
title : "Lindsay Martin"
description: ""
draft: false
weight: 3
---

{{< imageshortcode src="images/limartin.jpg" width="342" height="400">}}

Honestly, I just joined the student council because I wanted to help out. Codam's community is made up of students from all over the world and with all kinds of different backstories, but at the same time it's incredibly tight-knit and homely. I feel like that's also due to the innovative and inclusive values that Codam has, and I wanted to do my bit to make sure they're upheld.

Corona has been rough on everyone, and it was during the lockdown when students were sharing their personal struggles that I decided I'd like to work on solutions that work for everyone and leave no-one out.

As cheesy as I sound saying it, I really mean what I’ve said haha - so if there’s anything we can help with please do reach out!

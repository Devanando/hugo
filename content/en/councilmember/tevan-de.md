---
title : "Tessa van der Loo"
description: ""
draft: false
weight: 3
---

{{< imageshortcode src="images/tevan-de.jpg" width="342" height="400">}}

I’ve always made an effort to help out at Codam, because I believe in the peer to peer learning system and care about the community. I joined the C.A.T where I helped out during check ins and piscines and visited high schools to talk about Codam.


Joining the Student Council was a great opportunity for me to work in a small team of wonderful people who care as much about Codam as I do. Another thing that really motivated me to join the Student Council was seeing how many students were struggling with the remote curriculum, especially people who were dealing with mental health issues. I’d like to make an effort during my time on the Student Council to identify areas where students studying with mental health issues need extra support and work together with Bocal to implement this. 

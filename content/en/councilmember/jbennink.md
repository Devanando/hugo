---
title : "Jonas Bennink Bolt"
description: ""
draft: false
weight: 3
---

{{< imageshortcode src="images/jbennink.jpg" width="342" height="400">}}

I originally was interested in joining the council when a previous council member introduced the idea to me. The opportunity to be actively involved in working to try and make Codam even better really spoke to me. I think that with the council present to actively represent all the Codam students, we enable the staff to more easily consider the students’ point of view.
While the job of the treasurer in the council is of course not unimportant, it does not have many day-to-day activities, which allows me to be more involved in a wide variety of activities of the council. It really adds an extra layer to the whole experience of studying at Codam!

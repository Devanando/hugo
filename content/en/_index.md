---
title: "Home"
description: ""
images: ["logo.png"]
draft: false
menu: main
weight: 1
---

## Goal of the Codam Student Council

The student council is here to make sure student voices are heard, and offer staff an insight into what is going on at Codam. We aim to keep the quality of education high, as well as to improve the overall student experience.


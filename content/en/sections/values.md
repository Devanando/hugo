---
title : "Safeguarding Codam’s Values"
description: ""
draft: false
weight: 3
---

Making sure that a world-class coding education is available to everyone is part of Codam’s mission, and the student council shares this ambition. We are dedicated to making sure the school is an inclusive place to study, and offers equal opportunity to all its students regardless of their gender, sexual orientation, ethnic or cultural background, health, or any physical or mental conditions they may be living with.
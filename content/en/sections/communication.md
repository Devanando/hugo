---
title : "Clear Communication"
description: ""
draft: false
weight: 2
---

The council collects student opinions through surveys and focus groups to best represent the whole of the community. The council's advice can be delivered on the council's own initiative, based on an issue brought forward by a student at Codam, or asked for by the staff specifically. All this helps keep staff in touch with students and their studies.

The council is also committed to promoting transparency, openness, and frank discussion within Codam. We believe it is important that all students feel that they can express their opinions, and that Codam’s decision making should be explained as clearly as possible to them. As such, the council will also always represent students who wish to remain anonymous.

---
title : "Formal Representation"
description: ""
draft: false
weight: 1
---
**Formal representation**

The council meets with staff regularly and is a formal representation of the student’s interests on an executive level. Both staff and students want Codam to provide the best

possible education tailored towards the needs of IT and tech sectors, and the council represents the students’ perspective in this. Every term we actively work on a few topics that we think will improve the quality of studies most, and staff in turn takes this work seriously.

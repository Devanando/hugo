---
title : "Liewe Gutter"
description: ""
draft: false
weight: 2
---

{{< imageshortcode src="images/lgutter.jpg" width="400" height="400">}}

Codam has changed my life, and it has the potential to change many more lifes. But as can be expected from a new school, Codam has slowly been changing since it started, and I want to make sure these changes do not negatively impact its potential. This is, among others, a reason why I believe it is important that the student council can represent the students of Codam, and I feel I owe it to Codam and its students to help make this council a success.
---
title : "Tijmen T. El-Baradi"
description: ""
draft: false
weight: 1
---

{{< imageshortcode src="images/tel-bara.jpg" width="342" height="400">}}

Look, I’ll be honest, politics isn’t really my thing. I do believe, however, that new students, too, should have a voice at Codam. Our experiences and struggles are most likely different than those of the seasoned student. I don’t know whether I’m the right one for the job; but I’ll do my best to make sure our concerns, questions and opinions are shared and heard.
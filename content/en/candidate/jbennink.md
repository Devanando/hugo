---
title : "Tessa van der Loo"
description: ""
draft: false
weight: 3
---

{{< imageshortcode src="images/jbennink.jpg" width="342" height="400">}}

I try to be involved as much as possible in the Codam community to know what troubles people have with their work or with the system. I also try to speak up about these regularly (during the bocal standup). I think that creating the best possible environment will help out all of us in our time here and I hope I can continue to help make Codam an even better place from within the student council.
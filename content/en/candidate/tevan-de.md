---
title : "Tessa van der Loo"
description: ""
draft: false
weight: 3
---

{{< imageshortcode src="images/tevan-de.jpg" width="342" height="400">}}

Hi! My name is Tess! I want to represent Codam students as a member of the student council. Codam holds a very special place in my heart and I want to help create a safe and healthy learning environment for every Codam student. One of my focus points is to further improve support for students that are trying to navigate their student lives while dealing with mental or physical health issues.
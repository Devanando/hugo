---
title : "Lindsay Martin"
description: ""
draft: false
weight: 3
---

{{< imageshortcode src="images/limartin.jpg" width="342" height="400">}}

I've realised I'm a bit more opinionated on how things could be handled at Codam than I first thought haha, especially when it came to talking about the Corona measures at the Bocal stand-ups. I also love helping out in the community as a CAT, and hope I'd be an approachable point of contact for people who have ideas of their own.
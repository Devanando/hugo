---
title : "Saladin Lorenzo Afoh"
description: ""
draft: false
weight: 6
---

{{< imageshortcode src="images/safoh.jpg" width="342" height="400">}}

My purpose is to bring joy to learning for all students around the globe. While unsure on how to approach this, I believe Codam to be the first step. I'll put most of my efforts into further gamifying the Codam curriculum. Besides that, I'll continue to build on top of the newly made council, carrying my past experience as council member.
---
title : "Joanna Wytrzyszczewska"
description: ""
draft: false
weight: 5
---

{{< imageshortcode src="images/jowytrzy.jpg" width="400" height="400">}}

It's always a shame to see unfulfilled potential. Sometimes it feels that, instead of cherishing a rewarding cooperation, Codam and its students are stuck in a communication struggle. I believe a strong Student Council can help both Codam and the student community reach their ambitions and I would like to be a part of that.

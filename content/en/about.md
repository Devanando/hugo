---
title: "The Council"
description: ""
images: []
draft: false
menu: main
weight: 2
---
## Rights and Duties

The Codam Student Council Rights and Duties document is an agreement between the Council and the staff, describing the fundamentals of our cooperation. The main rights of the council as specified in the document are the rights to initiative, consultation, and information.

**Initiative**

The council is able to make suggestions to Codam staff on any subject concerning Codam, to which staff will reply (with their reasoning) within three months.

**Consultation**

The Codam staff will ask the student council for advice on matters that affect the continued existence of Codam and good course of affairs within Codam, specifically on matters that affect the students’ learning process. They agree to do so early enough such that the Council’s advice can effectively influence policy. The staff will get back to the council on how their advice has been taken into account as soon as possible. When the advice is not followed, a reasonable explanation will be provided and the Council will be able to have another discussion before the final decision is made.

**Information**

Codam agrees to inform the council unsolicited as possible about the matters that the council reasonably needs to know to be able to perform its duties. The staff will supply the council with information on any regulations and policies as made by 42 Paris that the council reasonably needs to know about to perform its duties, for example concerning education/examinations/quality control/privacy.

Besides this, it is also specified that:

- The Codam staff makes sure that the members of the council will not be disadvantaged within Codam because of their position in the council.

- The Codam staff will provide resources that the council will be able to use, including a budget for training and team building activities.

- Students can form independent committees to give advice to the student council, either on their own initiative or when consulted by the council.

## Things we work on

Here is a list of aspects we concern ourselves with:

● **Education** - _Everything a student has to deal with in order to graduate from Codam_ e.g. project, subjects, Black Holes + AGU’s (freezes), evaluations, internship (content)

● **Student support** - _Codam-provided resources that empower students to do well in their curriculum_  
e.g. assistance, counselling, internship support, workshops (curriculum- or internship-related), CodamX talks

● **Facilities** - _Tangible facilities and platforms_  
e.g. intra, tweek, guacamole and working from home set up, building: clusters, working stations, meeting rooms, auditorium, opening hours, access to the building, fruit, vending machines, Kanteen25 deals

● **Community -** _What helps to create identity and a sense of belonging at Codam_e.g. coalitions, student association/activity committee, social events at Codam, Codam values, vision and mission, communication style

● **External** - _Aspects involving external parties (not students and staff) but concerning students or future students_e.g. external relations - Codam board, 42 network, accreditations, admissions: online test, check-ins, piscines
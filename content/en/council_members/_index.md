---
title: "Current Term"
description: "Send us an email!"
images: []
draft: false
menu: main
weight: 4
---

## Current Priorities

**Project planning / structuring studies**

The council is working on gathering real historical data on the time investment required to complete each project and the rewards (experience / blackhole days) received for it. We intend to give the students a more accurate expectation of how much time they will have to spend on a project and what they will get back for completing it, hopefully allowing them more accuracy in planning their projects and managing their time. We intend to put all this information in an easy-to-read guide, available to all students.
 We also would like to see if it is possible to organise workshops to help people better plan their studies with the help of structures or techniques, giving every student the best possible chance of making it through the curriculum.

**Mental health**

The council wants to make sure that students that are studying with mental health problems get the same opportunities at Codam as any other student. When someone is affected by mental health problems, their performance at school can be impeded. This means they might be in need of a little more support and guidance. The council wants to investigate if students studying with mental health problems are currently receiving the support they need and is open for suggestions on anything that would help improve students&#39; well-being. In addition to getting feedback from students, we&#39;re looking into what kind of systems are in place at other educational institutions to support students with mental problems and we&#39;re reaching out to the expert centre on inclusive education for more information.

**Coalitions**

The student council is advising staff on improvements to the current coalition system.

We believe the coalitions have so much untapped potential as they span across every cohort and every existing friend-group at Codam. Because they are assigned at random they can connect students through activities or social events who might never really speak to each other otherwise. Community-building seems especially important now after what has been a very strange year, and we would like to see the coalitions thrive as mini-communities within Codam; as &#39;academic families&#39; or a support network you can reach out to, as well as just being the &#39;team&#39; you&#39;re proud to support.

**Intra features**

The Council learned that 42 is currently working on a new version of the intranet platform. We saw this as a great opportunity to suggest some improvements based on input from the students. Using the &quot;Weekly whiteboard question&quot; initiative in which students could anonymously express their ideas, we gathered feedback from Codam students. After aggregating it and discussing the ideas with the Council, we put together a list of suggestions and passed it on to the Codam staff. David received the proposal positively and forwarded the suggestion to 42.